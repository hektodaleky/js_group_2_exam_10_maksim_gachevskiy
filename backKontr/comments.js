const express = require('express');
const router = express.Router();


const createRouter = (db) => {

    router.get('/', (req, res) => {
        if (req.query.news_id) {
            db.query(`select* from comments where post_id=${req.query.news_id}`,
                function (error, results, fields) {
                    if (error) res.status(404).send("ПРоблема");
                    res.send(results);
                })
        }
        else
            db.query(`select* from comments`,
                function (error, results, fields) {
                    if (error) res.status(404).send("ПРоблема");
                    res.send(results);
                })
    });


    router.post('/', (req, res) => {

        const commentBody = req.body;
        if (!commentBody.author)
            commentBody.author = "Anonimus";
        if (!commentBody.comment) {
            res.status(404).send("Пустое поле");
            return;
        }
        db.query('insert into comments (post_id,author,comment) Values (?,?,?)',
            [commentBody.post_id, commentBody.author, commentBody.comment],
            function (error, results, fields) {
                if (error) res.status(404).send("ПРоблема");
                res.send(results);
            })

    });


    router.delete('/:id', (req, res) => {
        db.query(`delete from comments where id=${req.params.id}`,
            function (error, results, fields) {
                if (error) res.status(404).send("ПРоблема");
                res.send(results);
            });
    });


    return router;

};

module.exports = createRouter;