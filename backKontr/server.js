express = require('express');
mysql = require('mysql');
cors = require('cors');

const news = require('./news');
const comments = require('./comments');

const app = express();
app.use(express.json());
app.use(cors());

app.use(express.static('public'));
const port = 8000;


const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "carvet",
    database: 'news'
});

con.connect((err) => {
    if (err) throw err;
    app.use('/news', news(con));
    app.use('/comments', comments(con));
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});