const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('./config');
const router = express.Router();
const nanoid = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null, config.uploadPath);
    }, filename: (req, file, cd) => {
        cd(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});


const createRouter = (db) => {

    router.get('/', (req, res) => {

        db.query('select* from main_news',
            function (error, results, fields) {
                if (error) res.status(404).send("Нет такой буква");
                res.send(results);
            })
    });


    router.post('/', upload.single('image'), (req, res) => {
        const itemBody = req.body;
        console.log(itemBody);
        if (req.file) {
            itemBody.image = req.file.filename;
        }
        else {
            itemBody.image = null
        }


        db.query('insert into main_news (title,body,image) Values (?,?,?)',
            [itemBody.title, itemBody.news, itemBody.image],
            function (error, results, fields) {
                if (error) res.status(404).send("Имеются пустые данные");
                res.send(results);
            })

    });


    router.delete('/:id', (req, res) => {
        console.log(req.params.id);
        db.query(`delete from main_news where id=${req.params.id}`,
            function (error, results, fields) {
                if (error) res.status(404).send("Нет такой Новости");
                res.send(results);
            });
    });


    return router;

};

module.exports = createRouter;