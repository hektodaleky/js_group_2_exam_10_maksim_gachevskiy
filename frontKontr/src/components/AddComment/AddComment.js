import React from "react";
import "./AddComment.css";
const AddComment = props => {
    return (
        <div className="AddComment">
            <input placeholder="Author" value={props.authorValue} onChange={props.authorChange}/>
            <textarea placeholder="Comment" value={props.textValue} onChange={props.textChange}></textarea>
            <button onClick={props.sendComment}>Add comment</button>
        </div>
    )
};
export default AddComment;