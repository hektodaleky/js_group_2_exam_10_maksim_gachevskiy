import React from "react";
import "./InputForm.css";
const InputForm = props => {
    return (<div className="second-div">
        <div><label htmlFor="title">Title</label>
            <input className="title" onChange={props.changeTitle} value={props.titleValue}
                   id="title" type="text"></input>
        </div>
        <div><label htmlFor="inp-text">Input Text</label>
            <textarea className="news" onChange={props.changeText} value={props.newsValue}
                      id="inp-text" type="text"></textarea>
        </div>
        <input placeholder="Add file"
               type="file"
               name="image"
               onChange={props.change}
        />

        <button onClick={props.clickButton} id="addNews">Add News</button>

    </div>);
};
export default InputForm;