import React from "react";
import "./OneNews.css";
const OnePost = props => {
    return ( <div className="OneNews">
        {props.image ? <img alt='' width="100px" height="100px" src={"http://localhost:8000/uploads/" + props.image}/> : null}
        <div className="OnePost--second"><h5>{props.title}</h5>
            <p>{props.dateTime}</p>
            <i className="clicker" onClick={props.readFull}>Read Full Post >></i>
            <i className="clicker" onClick={props.delete}>Delete</i></div>
    </div>)
};
export default OnePost;