import React from "react";
import "./Comment.css";
const Comment = props => {
    return (
        <div className="Comment">
            <h6>{props.author}</h6>
            <p>{props.comment}</p>
            <i onClick={props.delete}>x</i>
        </div>
    )
};
export default Comment;