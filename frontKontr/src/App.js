import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router";
import AddNews from "./containers/AddNews";
import MainMenu from "./containers/MainMenu/MainMenu";
import FullPost from "./containers/FullPost/FullPost";

class App extends Component {
  render() {
    return (

        <Switch>


          <Route path='/add-news' exact component={AddNews}/>
          <Route path='/news/'  component={FullPost}/>
          <Route path='/' exact component={MainMenu}/>
          <Route render={() => <h1>Not found</h1>}/>

        </Switch>
    );
  }
}

export default App;
