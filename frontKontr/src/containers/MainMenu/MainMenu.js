import React, {Component} from "react";
import {connect} from "react-redux";

import "./MainMenu.css";
import {deleteNews, getNews, setPostId} from "../../store/action";
import OneNews from "../../components/OneNews/OneNews";
import {NavLink} from "react-router-dom";


class MainMenu extends Component {

    componentDidMount() {
        this.props.getNews();

    }

    showFullPost = (id) => {
        this.props.setPostId(id);
        this.props.history.push({
            pathname: '/news/' + id
        });

    };

    render() {

        let myNews = [];
        Object.keys(this.props.allNews).map((elem, i) => {
            let oneItem = {...this.props.allNews[elem]};
            myNews.push(<OneNews key={oneItem.id}
                                 image={oneItem.image}
                                 title={oneItem.title}
                                 dateTime={oneItem.date}
                                 delete={() => {
                                     this.props.deleteNews(oneItem.id)
                                 }}
                                 readFull={() => {
                                     this.showFullPost(oneItem.id)
                                 }}
            />);
            return null

        });
        return (
            <div>
                <p><NavLink className="AddPost" to="add-news" exact>Add News</NavLink></p>
                {myNews}
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        allNews: state.allNews
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getNews: () => dispatch(getNews()),
        deleteNews: (id) => dispatch(deleteNews(id)),
        setPostId: (id) => dispatch(setPostId(id))

    };
};
export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);