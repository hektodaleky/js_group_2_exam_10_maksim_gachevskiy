import React, {Component} from "react";
import {connect} from "react-redux";
import InputForm from "../components/InputForm/InputForm";
import {getNews, postData} from "../store/action";

class AddNews extends Component {

    state = {
        title: "",
        news: "",
        image: ""
    };

    changeTitle = event => this.setState({title: event.target.value});

    changeNews = event => this.setState({news: event.target.value});

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    submitData = (event) => {
        event.preventDefault();
        if (!this.state.news) {
            alert("Поле Title не должно быть пустым");
            return
        }
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.postData(formData);
        this.setState({title: "", news: '', image: ''});

        this.props.getNews(()=>{this.props.history.push({
            pathname: '/'
        });});
    };


    render() {

        return (


            <InputForm changeTitle={(event => this.changeTitle(event))}
                       changeText={(event => this.changeNews(event))}
                       clickButton={this.submitData}
                       titleValue={this.state.title}
                       newsValue={this.state.news}
                       change={this.fileChangeHandler}/>


        );
    }


}


const mapStateToProps = state => {
    return {

    }
};

const mapDispatchToProps = dispatch => {

    return {
        postData: (data) => dispatch(postData(data)),
        getNews:(event)=>dispatch(getNews(event))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNews);