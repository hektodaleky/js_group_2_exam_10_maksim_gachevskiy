import React, {Component} from "react";
import {connect} from "react-redux";
import "./FullPost.css";
import {deleteComment, getComments, postComment} from "../../store/action";
import AddComment from "../../components/AddComment/AddComment";
import Comment from "../../components/Comment/Comment";
class FullPost extends Component {
    state = {
        author: "",
        text: ''
    };
    goToMainMenu = () => {
        this.props.history.push({
            pathname: '/'
        });
    };


    sendComment = () => {
        if (!this.state.text) {
            alert("Поле комментарий не должно быть пустым!!!!!");
            return
        }

        this.props.postComment({
            post_id: this.props.post_id,
            author: this.state.author,
            comment: this.state.text
        }, this.props.post_id);
        this.setState({
            author: "",
            text: ""
        })
    };

    componentDidMount() {
        this.props.getComments(this.props.post_id)
    }
    ;

    changeAuthor = (event) => {
        this.setState({author: event.target.value});
    };
    changeText = (event) => {
        this.setState({text: event.target.value});
    };

    render() {

        const index = Object.keys(this.props.allNews).filter((item) => {
            return this.props.allNews[item].id === this.props.post_id
        });
        const post = this.props.allNews[index];
        if (!post) {
            return (
                <div className="Crutch" onClick={this.goToMainMenu}>
                    <i>GO TO MAIN MENU</i>
                </div>
            );
        }


        let myComments = [];
        Object.keys(this.props.comments).map((elem) => {
            let oneItem = {...this.props.comments[elem]};
            myComments.push(<Comment key={oneItem.id}
                                     author={oneItem.author}
                                     comment={oneItem.comment}
                                     delete={() => this.props.deleteComment(oneItem.id, this.props.post_id)}/>);
            return null

        });




            return (



                <div>
                    <div className="FullPost">
                        {post.image ?
                            <img alt="" width="200px" height="200px"
                                 src={"http://localhost:8000/uploads/" + post.image}/> : null}
                        <h3>{post.title}</h3>
                        <i className="dateTime">{post.date}</i>
                        <p>{post.body}</p>

                    </div>
                    <div>
                        <h5>Comments</h5>
                        <div className="AllComments">
                            {myComments}
                        </div>
                        <AddComment authorValue={this.state.author}
                                    textValue={this.state.text}
                                    authorChange={this.changeAuthor}
                                    textChange={this.changeText}
                                    sendComment={this.sendComment}/>
                    </div>
                </div>)
    }
}

const mapStateToProps = state => {

    return {
        allNews: state.allNews,
        post_id: state.post_id,
        comments: state.comments
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getComments: (id) => dispatch(getComments(id)),
        postComment: (data, id) => dispatch(postComment(data, id)),
        deleteComment: (id, post_id) => dispatch(deleteComment(id, post_id))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(FullPost);