import {COMMENT_SUCCESS, DATA_ERROR, DATA_REQUEST, DATA_SUCCESS, SET_ID} from "./action";
const initialState = {
        allNews: [],
        loading: false,
        error: false,
        post_id: 0,
        comments: []


    }
;
const reducer = (state = initialState, action) => {

    switch (action.type) {
        case DATA_REQUEST: {
            return {...state, loading: true, error: false};
        }
        case DATA_SUCCESS: {
            return {
                ...state,
                loading: false,
                error: false,
                allNews: action.data
            }
        }
        case COMMENT_SUCCESS: {
            return {
                ...state,
                loading: false,
                error: false,
                comments: action.data
            }
        }
        case DATA_ERROR: {
            return {...state, loading: false, error: true};
        }
        case SET_ID: {
            return {...state, post_id: action.id}
        }

        default:
            return state;
    }


};
export default reducer;