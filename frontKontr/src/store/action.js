import axios from "../axios";
export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const COMMENT_SUCCESS = 'COMMENT_SUCCESS';

export const DATA_ERROR = 'DATA_ERROR';
export const SET_ID = 'SET_ID';

export const dataRequest = () => {
    return {type: DATA_REQUEST}
};
export const dataSuccess = (data) => {
    return {type: DATA_SUCCESS, data}
};
export const commentsSuccess = (data) => {
    return {type: COMMENT_SUCCESS, data}
};
export const dataError = () => {
    return {type: DATA_ERROR}
};
export const getNews = (event) => {

    return (dispatch, getState) => {
        dispatch(dataRequest());
        axios.get('/news').then(response => {
            dispatch(dataSuccess(response.data));
            if (event)
                event();

        }, error => {
            dispatch(dataError())
        })
    }
};

export const getComments = (id) => {

    return (dispatch, getState) => {
        let myId;
        id ? myId = '/?news_id=' + id : myId = "";
        dispatch(dataRequest());
        axios.get('/comments' + myId).then(response => {
            dispatch(commentsSuccess(response.data));

        }, error => {
            dispatch(dataError())
        })
    }
};

export const postData = (data) => {
    const link = `/news`;
    return dispatch => {


        axios.post(link, data);

    }

};

export const postComment = (data, id) => {
    console.log(data, id, "ACTIIIIION")
    const link = `/comments`;
    return dispatch => {


        axios.post(link, data).then(() => {
            dispatch(getComments(id))
        });

    }

};


export const deleteNews = (id) => {
    const link = `/news/${id}`;
    return dispatch => {


        axios.delete(link).then(() => {
            dispatch(getNews())
        });

    }
};

export const deleteComment = (id, post_id) => {
    const link = `/comments/${id}`;
    return dispatch => {


        axios.delete(link).then(() => {
            dispatch(getComments(post_id))
        });

    }
};


export const setPostId = (id) => {
    return {type: SET_ID, id}
};
